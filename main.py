import os
import json
from flask import Flask, request, jsonify, render_template, make_response
from firebase_admin import credentials, firestore, initialize_app, auth
from datetime import datetime
import pyrebase
from functools import wraps
from validate_email import validate_email
from send_email import send_email
from firebase_logic import get_machine, get_machine_issues, get_machine_requests, get_user_machines

app = Flask(__name__)

# Initialize Firestore DB
cred = credentials.Certificate('key.json')
default_app = initialize_app(cred)
db = firestore.client()
pb = pyrebase.initialize_app(json.load(open('fbconfig.json')))

def check_token(f):
    @wraps(f)
    def wrap(*args,**kwargs):
        token = request.cookies.get('token')
        try:
            user = auth.verify_id_token(token)
            request.user = user
        except:
            return render_template('sorry.html', message="Invalid token provided", has_token=False),400
        return f(*args, **kwargs)
    return wrap

def has_token(request):
    token = request.cookies.get('token')
    try:
        user = auth.verify_id_token(token)
        return True
    except:
        return False

def get_user(request):
    token = request.cookies.get('token')
    try:
        user = auth.verify_id_token(token)
        return user
    except:
        return ''

def render_with_status(template_string, request, *args, **kwargs):
    token_status = has_token(request)
    return render_template(template_string, has_token=token_status, *args, **kwargs)

def machine_validation_issues(machine_ref, id, json):
    data = machine_ref.where(u'user_id', u'==', json['user_id']).stream()
    for datum in data:
        machine = datum.to_dict()
        if machine['location'] == json['location']:
            return 'You already have a vending machine with this name.'
    
    return False

def get_user_validation_issues(email, password):
    return validate_email(email, verify=True)

@app.route('/')
def home():
    return render_with_status('home.html', request)

@app.route('/new_request/<string:id>', methods=['GET'])
def new_request(id):
    location = id.split(' - ')[0]
    return render_with_status('request.html', request, id=id, location=location)

@app.route('/new_request/<string:id>', methods=['POST'])
def request_form(id):
    form = request.form
    request_text = form['request-text']
    if request_text == "":
        return render_with_status('request.html', request, id=id, error="Please specify your requested snack or drink", location=id.split(' - ')[0])

    json = { 'request': request_text, 'machine_id': id, 'created_at': datetime.now() }

    machine = get_machine(db, id)
    if machine == '':
        location = id.split(' - ')[0]
        return render_with_status('request.html', request, id=id, location=location, error="Unable to find this machine in our system, sorry")

    request_ref = db.collection('requests')
    request_ref.add(json)

    to = machine['email']
    location = machine['location']
    created_at = machine['created_at'].strftime('%b. %d, %Y')
    subject = 'New Request for Items at ' + location
    body = 'On ' + created_at + ', a patron requested: "' + request_text + '".'
    send_email(to, subject, body)

    return render_with_status('thanks.html', request, message="Your request has been relayed to the machine's owner!")

@app.route('/issue/<string:id>', methods=['GET'])
def issue(id):
    location = id.split(' - ')[0]
    return render_with_status('issue.html', request, id=id, location=location)

@app.route('/issue/<string:id>', methods=['POST'])
def issue_form(id):
    form = request.form
    request_text = form['request-text']
    if request_text == "":
        location = id.split(' - ')[0]
        return render_with_status('issue.html', request, id=id, error="Please specify the issue", location=location)
    
    json = { 'issue': request_text, 'machine_id': id, 'created_at': datetime.now() }

    machine = get_machine(id)
    if machine == '':
        location = id.split(' - ')[0]
        return render_with_status('issue.html', request, id=id, location=location, error="Unable to find this machine in our system, sorry")

    issue_ref = db.collection('issues')
    issue_ref.add(json)

    to = machine['email']
    location = machine['location']
    created_at = machine['created_at'].strftime('%b. %d, %Y')
    subject = 'New Issue reported at ' + location
    body = 'On ' + created_at + ', a patron reported: "' + request_text + '".'
    send_email(to, subject, body)

    return render_with_status('thanks.html', request, message="The issue has been reported to the machine's owner.")

@app.route('/machine/<string:id>', methods=['GET'])
@check_token
def machine(id):
    issues = get_machine_issues(db, id)
    requests = get_machine_requests(db, id)
    location = id.split(' - ')[0]
    return render_template('machine.html', has_token=True, issues=issues, requests=requests, location=location, id=id)


@app.route('/new_machine', methods=['GET'])
@check_token
def new_machine():
    return render_template('new_machine.html', has_token=True, error="")

@app.route('/new_machine', methods=['POST'])
@check_token
def new_machine_form():
    form = request.form
    extra_info = form['extra_info']
    location = form['location']
    user = get_user(request)
    user_id = user['user_id']
    email = user['email']
    json = { 'location': location, 'extra_info': extra_info, 'created_at': datetime.now(), 'user_id': user_id, 'email': email }

    machine_id = location + ' - ' + user_id
    machine_ref = db.collection('machines')
    validation_issues = machine_validation_issues(machine_ref, machine_id, json)
    if not validation_issues:
        machine_ref.document(machine_id).set(json)

        return render_template('machine_thanks.html', location=location, id=machine_id, has_token=True)
    return render_with_status('new_machine.html', request, error=validation_issues)

@app.route('/machines', methods=['GET'])
@check_token
def machines():
    try:
        user = get_user(request)
        user_id = user['user_id']
        user_machines = get_user_machines(db, user_id)
        return render_template('machines.html', has_token=True, machines=user_machines)
    except Exception as e:
        print(e)
        return render_with_status('sorry.html', request, message="We couldn't find any of your machines"),400

@app.route('/signup', methods=['GET'])
def signup():
    return render_with_status('signup.html', request)

@app.route('/signup', methods=['POST'])
def signup_form():
    email = request.form.get('email')
    password = request.form.get('password')
    user_validation_issues = get_user_validation_issues(email, password)
    if email is None or password is None:
        return {'message': 'Error missing email or password'},400
    try:
        user = auth.create_user(
               email=email,
               password=password
        )
        return render_with_status('thanks.html', request, message="You can now login to your account!"),200
    except Exception as e:
        error_message = 'Sorry, ' + str(e).replace(' (EMAIL_EXISTS)', '').replace('The', 'a')
        return render_with_status('signup.html', request, error=error_message),400

@app.route('/login', methods=['GET'])
def login():
    return render_with_status('login.html', request)

@app.route('/login', methods=['POST'])
def login_form():
    email = request.form.get('email')
    password = request.form.get('password')
    try:
        user = pb.auth().sign_in_with_email_and_password(email, password)
        jwt = user['idToken']
        
        resp = make_response(render_template('home.html', has_token=True))
        resp.set_cookie('token', jwt)

        return resp
    except Exception as e:
        error_message = 'Sorry, there was an error logging in: ' + str(e)
        if 'EMAIL_NOT_FOUND' in error_message:
            error_message = 'Sorry, we could not find any user with the provided email.'
        elif 'INVALID_PASSWORD' in error_message:
            error_message = 'Sorry, the provided password is invalid.'
        return render_with_status('login.html', request, error=error_message),400

@app.route('/signout')
def signout():
    resp = make_response(render_template('home.html', has_token=False))
    resp.set_cookie('token', '', expires=0)
    return resp

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8080, debug=True)