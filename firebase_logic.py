def get_machine(db, id):
    machine_ref = db.collection('machines')
    data = machine_ref.document(id).get()
    if data:
        machine = data.to_dict()
        machine['id'] = data.id
        return machine
    else:
        print("Couldn't find machine in database that was requested")

    return ''

def get_user_machines(db, user_id):
    machine_ref = db.collection('machines')
    user_machines = []
    data = machine_ref.where(u'user_id', u'==', user_id).stream()
    for datum in data:
        machine = datum.to_dict()
        machine['id'] = datum.id
        user_machines.append(machine)
    return user_machines

def get_machine_issues(db, machine_id):
    issue_ref = db.collection('issues')
    issues = issue_ref.where(u'machine_id', u'==', machine_id).stream()
    issues_result = []
    for issue in issues:
        issues_result.append(issue.to_dict())
    return issues_result

def get_machine_requests(db, machine_id):
    requests_ref = db.collection('requests')
    requests = requests_ref.where(u'machine_id', u'==', machine_id).stream()
    requests_result = []
    for req in requests:
        requests_result.append(req.to_dict())
    return requests_result